## Status Notifier for Services Hosted by FSCI

URL: https://status.fsci.in

### Requirements:
- Git
- [hugo](https://gohugo.io)

### Setup:

Clone this repo 

- `git clone --recursive https://gitlab.com/fsci/status.git`

- `cd` into the folder

**To create a new issue:**
> If you are familiar with hugo, Creating an issue is similar to creating a blog post.

- `hugo new issues/<title-of-the-issue.md>`. Suggested format for title `20210428-<SERVICE_URL>-<PACKAGE_VERSION_IF_APPLICABLE>-<NOTICE-TYPE>.md`.

-  Now, you can edit the file in `content/issues/<title-of-the-issue>.md` various fields in the file are self explanatory

- You can preview the page by running `hugo server -D` & opening `localhost:1313`
in your web browser.

- git commit & push your changes to this repo.



For more details, Please visit https://github.com/cstate/cstate
