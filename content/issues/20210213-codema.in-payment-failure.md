---
title: "codema.in down due to payment failure"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-02-13 18:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-02-13 20:00:00
# You can use: down, disrupted, notice
severity: down
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
- loomio
# - videos

# Don't change the value below
section: issue
---
