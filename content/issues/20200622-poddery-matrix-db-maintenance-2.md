---
title: "Poddery Matrix Database Maintenance - Second Phase"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-06-22 23:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-06-23 01:30:00
# You can use: down, disrupted, notice
severity: down
# affected sections (array). Uncomment the affected one's
affected:
 - matrix
# - xmpp
# - diaspora
# - gitlab
# - webiste
# - mailing-lists
# - loomio

# Don't change the value below
section: issue
---
The matrix synapse server was down for performing various database optimization operations such as removing empty rooms, compressing data, etc.
