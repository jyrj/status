---
title: "Poddery Matrix Database Maintenance"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-06-22 15:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-06-22 18:30:00
# You can use: down, disrupted, notice
severity:
# affected sections (array). Uncomment the affected one's
affected:
 - matrix
# - xmpp
# - diaspora
# - gitlab
# - webiste
# - mailing-lists
# - loomio

# Don't change the value below
section: issue
---
Monday 22/06/2020 starting from 15:30 to 18:30 we will work on some database maintenance issues, so you may experience some downtime between 1 to 3 hours. Services affected:
* Matrix
