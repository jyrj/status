---
title: "Poddery Matrix Synapse Update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-06-20 23:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-06-21 00:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos

# Don't change the value below
section: issue
---
Sunday 20/06/2021 23:00 IST is scheduled for updating synapse matrix server. The process may take upto 1 hour depending on the maintenance required and you may experience some downtime during this period.

NOTE: Synpase not upgraded due to some technical issues. The currently running version 1.20.1 is very old and needs more time and care than anticipated. This will be fixed soon.