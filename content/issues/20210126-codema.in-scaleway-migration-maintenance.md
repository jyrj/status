---
title: "codema.in scheduled maintenance by scaleway"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-01-26 20:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-01-26 21:30:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
- loomio
# - videos

# Don't change the value below
section: issue
---
A notice of maximum 1h downtime is issued by Scaleway due to their migration maintenance. See https://status.scaleway.com for more details.
