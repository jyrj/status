---
title: "Gitlab 13.4.7 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-12-10 00:50:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-12-10 01:40:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated gitlab to 13.4.7 security release.
