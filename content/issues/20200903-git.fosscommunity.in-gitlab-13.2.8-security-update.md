---
title: "Gitlab 13.2.8 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-09-03 21:55:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-09-03 22:35:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated gitlab to 13.2.8 security release. We also switched ruby on rails
application server from unicorn to puma (improves performance).
